import React from "react";
import { Wrapper, Button } from "../../styles/home";

const axios = require("axios");

const Home = () => {
    const handleClick = () => {
        axios.get("http://localhost:8000/voice").then((resp) => {
            console.log(resp.data);
        });
    };
    const advanceClick = () => {
        axios.get("http://localhost:8000/advance").then((resp) => {
            console.log(resp.data);
        });
    };
    return ( <
        >
        <
        Wrapper >
        <
        Button onClick = {
            () => handleClick() } > Speak < /Button>{" "} <
        Button onClick = {
            () => advanceClick() } > advance tool < /Button>{" "} <
        /Wrapper>{" "} <
        />
    );
};

export default Home;